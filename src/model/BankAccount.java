package model;

import interfaces.Measurable;

public class BankAccount implements Measurable{
	private double balance;
	
	public BankAccount(double abalance){
		balance = abalance;
	}
	public double getMeasure(){
		return balance;
	}
}
