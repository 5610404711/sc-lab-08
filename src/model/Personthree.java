package model;

import interfaces.Taxable;

public class Personthree implements Taxable {
	
	private String name;
	private double salary;

	public Personthree(String name,double sl){
		this.name = name;
		salary = sl;
	}

	@Override
	public double getTax() {
		double tax = 0;
		if(salary<=300000){
			 tax = (salary*5)/100;
		}else{
			double one = 15000;
			double two = salary-300000;
			tax = one+((two*10)/100);
		}
		return tax;
	}

}
