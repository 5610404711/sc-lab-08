package model;

import interfaces.Taxable;

public class Company implements Taxable{
	private String cname;
	private double income;
	private double outcome;
	
	public Company(String cname, double income, double outcome) {
		this.cname = cname;
		this.income = income;
		this.outcome = outcome;
	}
	
	public double getTax(){
		double ans = income - outcome;
		double tax = (ans*30)/100;
		return tax;
	}

}
