package model;

import interfaces.Taxable;

public class Product implements Taxable{
	private String pname;
	private double price;
	
	
	public Product (String pname, double price){
		this.pname = pname;
		this.price = price;
	}
	

	public double getTax(){
		double tax = (price*7)/100;
		return tax;
	}
}
