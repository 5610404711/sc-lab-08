package controller;

import java.util.ArrayList;

import interfaces.Measurable;
import interfaces.Taxable;
import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Personthree;
import model.Product;
import model.TaxCalculator;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test test = new Test();
		test.testPerson();
		test.testMin();
		test.testTax();
	}
	
	
	//Test1
	public void testPerson(){
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("Donald",170);
		persons[1] = new Person("Mary",165);
		persons[2] = new Person("Ricky",187);
		System.out.println("Test1 :");
		System.out.println(Data.average(persons));
		System.out.println("\n");
		
	}
	
	//Test2
	public void testMin(){
		Measurable[] acc = new Measurable[2];
		acc[0] = new BankAccount(20000);
		acc[1] = new BankAccount(50000);
		Measurable minAcc = Data.min(acc[0],acc[1]);
		System.out.println("Test2 :");
		System.out.println(minAcc.getMeasure());
		
		Measurable[] ct = new Measurable[2];
		ct[0] = new Country("Australia",9800000);
		ct[1] = new Country("Taiwan",7000000);
		Measurable minCt = Data.min(ct[0],ct[1]);
		System.out.println(minCt.getMeasure());
		
		Measurable[] ps = new Measurable[2];
		ps[0] = new Person("Micky",178);
		ps[1] = new Person("Minney",167);
		Measurable minPs = Data.min(ps[0],ps[1]);
		System.out.println(minPs.getMeasure());
		System.out.println("\n");
		
	}
	
	//Test3
	public void testTax(){
		ArrayList<Taxable> plist = new ArrayList<Taxable>();
		plist.add(new Personthree("Julie", 100000));
		plist.add(new Personthree("John", 500000));
		System.out.println("Test3 :");
		System.out.println(TaxCalculator.sum(plist));
		
		ArrayList<Taxable> clist = new ArrayList<Taxable>();
		clist.add(new Company("CCM", 1000000, 800000));
		clist.add(new Company("TNM", 1400000, 910000));
		System.out.println(TaxCalculator.sum(clist));
		
		ArrayList<Taxable> dlist = new ArrayList<Taxable>();
		dlist.add(new Product("Popcorn",300));
		dlist.add(new Product("Chips",450));
		System.out.println(TaxCalculator.sum(dlist));
		
		ArrayList<Taxable> alllist = new ArrayList<Taxable>();
		alllist.add(new Personthree("Julie", 100000));
		alllist.add(new Company("CCM", 1000000, 800000));
		alllist.add(new Product("Popcorn",300));
		System.out.println(TaxCalculator.sum(alllist));
		
	}
}

